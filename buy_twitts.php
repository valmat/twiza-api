#!/usr/bin/php
<?php

function print_help($scrname)
{
    echo PHP_EOL, 'usege: '. $scrname, ' -t <TWIT_TEXT> [-c <COUNT>] [-y]';
    echo PHP_EOL, 'exemple: '. $scrname, ' -t"My awesome twitt" -c3';
    
    echo PHP_EOL, '-t twit text';
    echo PHP_EOL, '-c count (default 1)';
    echo PHP_EOL, '-y twitting accaunt is in Yandex index (default: NO)';
    echo PHP_EOL, '-h print current help and exit';
    echo PHP_EOL;
    exit;
}
$options = getopt("t:c::y::h");
if(isset($options['h']) || !isset($options['t']) ) {
    print_help($argv[0]);
}

require 'api.php';

$text      = $options['t'];
$count     = isset($options['c']) ? (int)$options['c'] : 1;;
$in_yandex = isset($options['y']) ? Twiza\Api::IN_YANDEX_YES : Twiza\Api::IN_YANDEX_NO;


list($login, $psw) = explode("\n", trim(file_get_contents('auth_data.txt')), 2);

$twiza = new Twiza\Api($login, $psw);

echo $twiza->toTwitt($text, $count, $in_yandex);