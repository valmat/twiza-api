#!/usr/bin/php -d memory_limit=64M
<?php


function print_help($scrname)
{
    echo PHP_EOL, 'usege: '. $scrname, ' -t <TWIT_ID> [-c <COUNT>]'; 
    echo PHP_EOL, 'exemple: '. $scrname, ' -t649908862594719744 -c3';
    echo PHP_EOL, 'exemple: '. $scrname, ' -thttps://twitter.com/rublecur/status/649909085933043712 -c2 -f';
    
    echo PHP_EOL, '-t twit_id or twiit url';
    echo PHP_EOL, '-c count (default 1)';
    echo PHP_EOL, '-f add to favorites';
    echo PHP_EOL, '-h print current help and exit';
    echo PHP_EOL;
    exit;
}
$options = getopt("t:c::f::u::h");
if(isset($options['h']) || !isset($options['t']) ) {
    print_help($argv[0]);
}
$twi_id   = $options['t'];
$count    = isset($options['c']) ? (int)$options['c'] : 1;;
$addToFav = isset($options['f']);



list($login, $psw) = explode("\n", trim(file_get_contents('auth_data.txt')), 2);

require 'api.php';

$twiza = new Twiza\Api($login, $psw);

echo $twiza->toRetwit($twi_id, $count, true, $addToFav);
