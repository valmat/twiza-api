#!/usr/bin/php -d memory_limit=64M
<?php

function print_help($scrname)
{
    echo PHP_EOL, 'usege: '. $scrname, ' -t <SCREEN_NAME> [-c <COUNT>]'; 
    echo PHP_EOL, 'exemple: '. $scrname, ' -trublecur -c55';
    echo PHP_EOL, '-t screen_name';
    echo PHP_EOL, '-c count (default 1)';
    echo PHP_EOL, '-h print current help and exit';
    echo PHP_EOL;
    exit;
}
$options = getopt("t:c::h");
if(isset($options['h']) || !isset($options['t']) ) {
    print_help($argv[0]);
}
$scr_nm  = $options['t'];
$count   = isset($options['c']) ? (int)$options['c'] : 1;;



list($login, $psw) = explode("\n", trim(file_get_contents('auth_data.txt')), 2);

require 'api.php';

$twiza = new Twiza\Api($login, $psw);

echo $twiza->toFollow($scr_nm, $count);
