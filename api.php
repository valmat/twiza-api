<?php
/**
  *  
  */
/**
 *  API for twiza.ru
 *
 *  @author   Valeriy Dmitriev aka valmat <ufabiz@gmail.com>, http://valmat.ru/
 *  @licenses MIT https://opensource.org/licenses/MIT
 *  @repo     https://bitbucket.org/valmat/twiza-api
 */
  
namespace Twiza;

class Api{
    
    /**
      *  "In Yandex" flag.
      */
    const IN_YANDEX_YES     = 1;
    const IN_YANDEX_NO      = 0;
    
    /**
      *  The ini file, contains cookies 
      */
    private $coo_ini = 'cookies.ini';
    /**
      *  The authorization cookie as an array
      */
    private $coo_arr = [];
    /**
      *  The authorization cookie as an string
      */
    private $coo_str = '';
    /**
      *  The object that contains user informations
      */
    private $user;
    
    function __construct($login, $psw, $cookies_ini_file = NULL) {
        
        if($cookies_ini_file) {
            $this->coo_ini = $cookies_ini_file;
        }
        
        // Если файл с ключами отсутствует, авторизуемся и создаем его
        if(!is_file($this->coo_ini))
        {
            $this->auth($login, $psw);
        }
        // В противном случае авторизуемся по нему
        else
        {
            $coo_entries   = file_get_contents($this->coo_ini);
            $this->coo_arr = explode("\n", $coo_entries);
            $this->coo_str = join('; ', $this->coo_arr);
        }
        
        $this->user = $this->getUser();
        
        // Если авторизационная информация не актуальна, переавторизовываемся
        if(false === $this->user)
        {
            $this->auth($login, $psw);
            $this->user = $this->getUser();
        }
    }
    
    /**
      *  Баланс
      *  @return int
      */
    function balance() {
        if(!isset($this->user->funds)) return NULL;
        return  (int)$this->user->funds;
    }

    /**
      *  Последние заказы на фолловинг
      *  @param int count
      *  @param int page nomber
      */
    function followLastOrders($count = 10, $page = 1) {
        return $this->getOrders('following', $count, $page);
    }
    /**
      *  Последние заказы на ритвитинг
      *  @param int count
      *  @param int page nomber
      */
    function retwitLastOrders($count = 10, $page = 1) {
        return $this->getOrders('retwitting', $count, $page);
    }
    /**
      *  Последние заказы на прогон ссылок
      *  @param int count
      *  @param int page nomber
      */
    
    function totwitLastOrders($count = 10, $page = 1) {
        return $this->getOrders('linkstraining', $count, $page);
    }
    
    // Получаем информацию по добавленному заданию
    function getCurrent($orders, $title) {
	$current = current($orders);
	$cntr = 0;
	while($current->title !== $title && $cntr < 10) {
	    next($orders);
	    $current = current($orders);
	    $cntr++;
	}
	return $current;
    }
    
    /**
      *  Покупка фолловеров
      *  @param $scr_name     string (or array) username at twitter
      *  @param $count        int count of followers
      *  @param $autoActivate bool if is needs to activate order automaticly
      *  @return int order id 
      */
    function toFollow($scr_name, $count, $autoActivate = true) {
        
        if(is_array($scr_name)) {
            $scr_name = join("\n", $scr_name);
            $title = 'Follow_list [' . count($scr_name) . " -> $count] @" . microtime(1);
        } else {
            $title = $scr_name . " [$count] @" . microtime(1);
        }
        
        // Добавляем задание
        $orders = $this->post('following', [
            'AddAccsToFollowForm[repeat_times]'  => $count,
            'AddAccsToFollowForm[import_text]'   => $scr_name,
            'AddAccsToFollowForm[project_title]' => $title,
        ])->data->orders;
        
        $order_id = $this->getCurrent($orders, $title)->order_id;
        
        // Активируем задание
        if($autoActivate) {
           $this->activateOrder($order_id);
        }
        
        return $order_id;
    }
    
    /**
      *  Покупка ретвитов
      *  @param $scr_name         string (or array) username at twitter
      *  @param $count            int count of followers
      *  @param $autoActivate     bool if is needs to activate order automaticly
      *  @param $add_to_favorites bool if is needs to add to favorite
      *  @return int order id 
      *  
      */
    function toRetwit($twi, $count = 1, $autoActivate = true, $add_to_favorites = false) {
        
        if(is_array($twi)) {
            $twi = array_map(function($item) {
                if(!preg_match ( '/^https?\:\/\/twitter\.com\/\w+\/status\/\d+\/?$/i' , $item)) {
                    return 'https://twitter.com/twitter/status/' . $item;
                }
                return $item;
            }, $twi);
            $twi_str = join("\n", $twi);
            $title = 'Twi_list [' . count($twi) . '] @' . microtime(1);
        } elseif(is_string($twi) && preg_match ( '/^https?\:\/\/twitter\.com\/\w+\/status\/(\d+)\/?$/i' , $twi, $matches)) {
            $twi_str = $twi;
            $title = $matches[1] . ' @' . microtime(1);
        } else {
            $twi_str = 'https://twitter.com/twitter/status/' . $twi;
            $title = $twi . ' @' . microtime(1);
        }
        
        // Добавляем задание
        $orders  = $this->post('retwitting', [
            'AddTwittsForRetwittForm[repeat_times]'     => $count,
            'AddTwittsForRetwittForm[import_text]'      => $twi_str,
            'AddTwittsForRetwittForm[project_title]'    => $title,
            'AddTwittsForRetwittForm[add_to_favorites]' => $add_to_favorites ? '1' : ''
        ])->data->orders;
        
        $order_id = $this->getCurrent($orders, $title)->order_id;
        
        // Активируем задание
        if($autoActivate) {
           $this->activateOrder($order_id);
        }
        
        return $order_id;
    }
    
    
    /**
      *  Покупка фолловеров
      *  @param $scr_name     string (or array) username at twitter
      *  @param $count        int count of followers
      *  @param $autoActivate bool if is needs to activate order automaticly
      *  @return int order id
      */
    function toTwitt($text, $count = 1, $in_yandex = self::IN_YANDEX_NO, $autoActivate = true) {
        
        if(is_array($text)) {
            $text = join("\n", $text);
        }
        
        // Добавляем задание
        $orders = $this->post('linkstraining', [
            'AddLinksForm[repeat_times]'    => $count,
            'AddLinksForm[yandex_index]'    => $in_yandex,
            'AddLinksForm[followers_count]' => '10-50',
            'AddLinksForm[import_text]'     => $text,
        ])->data->orders;
        
        $order_id = $this->getCurrent($orders, NULL)->order_id;
        
        // Активируем задание
        if($autoActivate) {
           $this->activateOrder($order_id);
        }
        
        return $order_id;
    }
    
    /**
      *  deprecated
      *  Активируем задание для ретвита
      *  @param $order_id  int
      *  @return bool 
      */
    function activateRetwit($order_id) {
        trigger_error('`activateRetwit` is deprecated method. Use activateOrder() instead', E_USER_NOTICE);
        return $this->activateOrder($order_id);
    }
    
    /**
      *  deprecated
      *  Активируем задание для подписки
      *  @param $order_id  int
      *  @return bool 
      */
    function activateFollow($order_id) {
        trigger_error('`activateFollow` is deprecated method. Use activateOrder() instead', E_USER_NOTICE);
        return $this->activateOrder($order_id);
    }
    
    /**
      *  Активируем задание
      *  @param $action    string action: 'retwitting' or 'following'
      *  @param $order_id  int
      *  @return bool 
      */
    function activateOrder($order_id) {
        $rez = $this->get('retwitting', ['op' => 'pay', 'order_id' => $order_id]);
        return !isset($rez->messages->error);
    }
    
    function removeOrder($order_id) {
        $rez = $this->get('retwitting', ['op' => 'remove', 'order_id' => $order_id]);
        return isset($rez->messages->success);
    }
    
    /**
      *  Удаляет неактивные заказы для ретвитов
      *  @param int count
      *  @param int page nomber
      */
    function removeInactiveRetwitt($count = 10, $page = 1) {
        $this->removeInactive('retwitting', $count, $page);
    }
    
    /**
      *  Удаляет неактивные заказы для подписки
      *  @param int count
      *  @param int page nomber
      */
    function removeInactiveFollow($count = 10, $page = 1) {
        $this->removeInactive('following', $count, $page);
    }
    
    
    /**
      *  Получаем информацию о пользователе, от которого делаются запросы 
      */
    private function getUser() {
        $src = $this->get('billing');
        if(isset($src->session_expired) || !isset($src->user)) {
            return false;
        }
        return $src->user;
    }
    
    /**
      *  авторизация по логину и паролю 
      */
    private function auth($login, $psw) {
        $data = $this->data2str(array(
            'LoginForm[email]'      => $login,
            'LoginForm[password]'   => $psw,
            'LoginForm[rememberMe]' => 1
        ));
        
        $opts = array(
            'http'=>array(
                'method'=>"POST",
                'header'=>  "Host:twiza.ru\r\n" .
                            "User-Agent: Twiza-API v-0.01\r\n" .
                            "Accept:text/html, */*; q=0.01\r\n" .
                            "Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3\r\n" .
                            "Origin:https://twiza.ru\r\n" .
                            "Referer:https://twiza.ru/\r\n" .
                            "Content-Type:application/x-www-form-urlencoded; charset=UTF-8\r\n" .
                            "Pragma:no-cache\r\n" .
                            "X-Requested-With:XMLHttpRequest\r\n" .
                            "Cache-Control:no-cache\r\n" .
                            "Content-Length: " . strlen($data) . "\r\n",
                'content' => $data
            )
        );
        stream_context_set_default($opts);
        $headers = get_headers('https://twiza.ru/auth/login', 1);
        
        
        $coo_src = array_map(function($a) {return parse_ini_string($a);}, $headers['Set-Cookie']);
        $coo = [];
        foreach($coo_src as $coo_item) {
            $coo = array_merge($coo, $coo_item);
        }
        
        $this->coo_arr = [];
        foreach($coo as $coo_name => $coo_val) {
            $this->coo_arr[] = "$coo_name=$coo_val";
        }
        
        file_put_contents($this->coo_ini, join("\n", $this->coo_arr));
        $this->coo_str = join('; ', $this->coo_arr);
    }
    
    /**
      *  Удаляет неактивные заказы
      *  @param $action    string action: 'retwitting' or 'following'
      *  @param int count
      *  @param int page nomber
      */
    private function removeInactive($action, $count = 10, $page = 1) {
        $orders = ('following' == $action) ?  $this->followLastOrders($count, $page) : $this->retwitLastOrders($count, $page);
        
        foreach($orders as $order) {
            if(!$order->active) {
                $this->removeOrder($order->order_id);
            }
        }
    }
   
    
    /**
      *  Получить список созданных заказов
      *  @param $action    string action: 'retwitting' or 'following'
      *  @param int count
      *  @param int page nomber
      */
    private function getOrders($action, $count = 10, $page = 1) {
        return $this->post($action, [
            'page'  => $page,
            'count' => $count
        ])->data->orders;
    }
    
    /**
      *  POST request
      */
    private function post($url, array $dataArr) {
        $data = $this->data2str($dataArr);
        $opts = array(
            'http'=>array(
                'method'=>"POST",
                'header'=>  "Host:twiza.ru\r\n" .
                            "User-Agent: Twiza-API v-0.01\r\n" .
                            "Accept:application/json, text/plain, */*\r\n" .
                            "Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n" .
                            "Origin:https://twiza.ru\r\n" .
                            "Referer:https://twiza.ru/customer\r\n" .
                            "Content-Type:application/x-www-form-urlencoded; charset=UTF-8\r\n" .
                            "Pragma:no-cache\r\n" .
                            "X-Requested-With:XMLHttpRequest\r\n" .
                            "Cache-Control:no-cache\r\n" .
                            "Cookie:{$this->coo_str}\r\n" .
                            "Content-Length: " . strlen($data) . "\r\n",
                'content' => $data
            )
        );
        return json_decode(file_get_contents("https://twiza.ru/customer/$url", false, stream_context_create($opts)));
    }
    
    /**
      *  GET request
      */
    private function get($url, array $dataArr = array()) {
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>  "Host:twiza.ru\r\n" .
                            "User-Agent: Twiza-API v-0.01\r\n" .
                            "Accept:application/json, text/plain, */*\r\n" .
                            "Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n" .
                            "Referer:https://twiza.ru/customer\r\n" .
                            "Pragma:no-cache\r\n" .
                            "X-Requested-With:XMLHttpRequest\r\n" .
                            "Cache-Control:no-cache\r\n" .
                            "Cookie:{$this->coo_str}\r\n"
            )
        );
        $url = 'https://twiza.ru/customer/' . $url;
        if(count($dataArr)) {
            $url .= '?'.$this->data2str($dataArr);
        }
        return json_decode(file_get_contents($url, false, stream_context_create($opts)));
    }
    
    /**
      *  Кодирует данные для отправки их в POST запросе
      */
    private function data2str(array $data) {
        foreach($data as $k => $v) {
            $data[$k] = "$k=$v";
        }
        return implode('&', $data);
    }
    
}

